import {useEffect} from 'react';

type FilthyAdProps = { slot: string };

export const FilthyAd = ({ slot }: FilthyAdProps) => {
    useEffect(() => {
        // @ts-ignore
        (window.adsbygoogle = window.adsbygoogle || []).push({});
    }, []);

    return (
            <ins
                className="adsbygoogle"
                style={{ display: 'block', maxHeight: '135px', height: '135px' }}
                data-full-width-responsive="true"
                data-ad-client="ca-pub-7353288407516672"
                data-ad-slot={slot}
                data-ad-format="Horizontal"
                >
            </ins>
    );
}
