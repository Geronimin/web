export interface WeaponStats {
    player_name: string;
    weapons:     WeaponStat[];
}

export interface WeaponStat {
    weapon:             string;
    weapon_name:        string;
    usages:             number;
    average_item_power: number;
    kill_fame:          number;
    death_fame:         number;
    kills:              number;
    deaths:             number;
    assists:            number;
    fame_ratio:         number | null;
    win_rate:           number;
}
