export interface PlayerInfo {
    name: string;
    elo_1v1_slayer?: number;
    rank_1v1_slayer?: number;
    elo_1v1_stalker?: number;
    rank_1v1_stalker?: number;
    rating_2v2?: number;
    rank_2v2?: number;
    fav_weapon?: string;
    fav_weapon_1v1?: string;
    fav_weapon_2v2?: string;
    twitch_name?: string;
}


export interface Player2V2BattleInfo {
    partners: Partner[];
    weapons: Weapons[];
}

export interface Partner {
    name: string;
    count: number;
    wins: number;
    win_rate: number;
}

export interface Weapons {
    weapon_1: string;
    weapon_2: string;
    wins: number;
}
