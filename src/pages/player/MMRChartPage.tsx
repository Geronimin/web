import { useEffect, useState } from 'react';
import { Space } from 'antd';
import { Line } from '@ant-design/charts';

import { api } from 'src/api'
import { Datum } from 'src/models/timeseries';

type MMRChartPageProps = {
    name: string,
}

export const MMRChartPage = ({ name }: MMRChartPageProps) => {
    const [data, setData] = useState<Datum[]>([]);
    const [max, setMax] = useState<number>(0);

    useEffect(() => {
        api<{ data: Datum[] }>(`/players/${name}/elo-chart`)
            .then(r => {
                setData(r.data);
                setMax(r.data.reduce((m, c) => c.value > m ? c.value : m, 0));
            });
    }, [name]);

    const config = {
        data,
        xField: 'time',
        yField: 'value',
        meta: {
            value: { alias: 'MMR' }
        },
        point: {
            size: 5,
            shape: 'circle',
        },
        label: {
            style: {
                fill: '#ff3333',
            },
        },
        slider: {
            start: 0.5,
            end: 1,
        },
        color: '#ff0000',
    };

    return (
        <Space direction="vertical" style={{ width: '100%' }}>
            <span>Max: {max}</span>
            <Line {...config} />
        </Space>
    );
}
