import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    css: {
        preprocessorOptions: {
            less: {
                modifyVars: {
                    'primary-color': '#bd6c33',
                    'body-background': '#090909',
                },
                javascriptEnabled: true,
            },
        }
    },
    resolve: {
        alias: [
            { find: /^~/, replacement: '' },
            { find: /^src/, replacement: path.resolve('src/') },
        ],
    }
})
